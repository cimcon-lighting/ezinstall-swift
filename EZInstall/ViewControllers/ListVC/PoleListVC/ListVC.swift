//
//  ListVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 06/07/21.
//

import UIKit

class CellList : UITableViewCell {
    @IBOutlet var slcidHeading      : UILabel!
    @IBOutlet var poleidHeading     : UILabel!
    @IBOutlet var addressHeading    : UILabel!
    @IBOutlet var addedonHeading    : UILabel!
    
    @IBOutlet var addedonLabel      : UILabel!
    @IBOutlet var addressLabel      : UILabel!
    @IBOutlet var poleidLabel       : UILabel!
    @IBOutlet var slcidLabel        : UILabel!
    
}

class ListVC: UIViewController {
    
    var listModel                       = ListViewModel()
    
    @IBOutlet var tblData               : UITableView!
    
    @IBOutlet var slcidTextfield        : UITextField!
    @IBOutlet var slctypeTextfield      : UITextField!
    @IBOutlet var frmDateTextfield      : UITextField!
    @IBOutlet var toDateTextfield       : UITextField!
    
    @IBOutlet var consSearchHeight      : NSLayoutConstraint!
    
    var isFilterOpen                    : Bool! = false
    
    var arrTbldata      : [List] = []
    
    var pagno           : Int = 1
    var totalSlccount   : Int = 1
    
    var arrSLCtype  = ["All", "Internal", "External"]
    
    var picker          : UIPickerView!
    var toolBar         : UIToolbar!
    let datePicker      = UIDatePicker()
    
    var isFrmDtPicker   : Bool!
    
    private let refreshControl = UIRefreshControl()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        consSearchHeight.constant = 0
        
        loadData()
        pickerView()
        
        refreshControl.tintColor = UIColor.init(hexString: ThemeColor.THEME_BLUE_COLOR)
        tblData.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apicalling()
    }
    
    func loadData() {
        pagno = 1
        totalSlccount = 1
        arrTbldata.removeAll()
        tblData.reloadData()
        
        let toDate = Date().string(format: DATE_FORMAT)
        let fromDate = Date().addDays(daysToAdd: -6).string(format: DATE_FORMAT)
        
        slcidTextfield.text     = ""
        frmDateTextfield.text   = fromDate
        toDateTextfield.text    = toDate
        slctypeTextfield.text   = arrSLCtype.first
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        clearButton_Click(sender: UIButton())
    }
    
    func pickerView() {
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 300))
        picker.backgroundColor = .white
        
        picker.delegate = self
        picker.dataSource = self
        
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.init(hexString: ThemeColor.THEME_BLUE_COLOR)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        slctypeTextfield.inputView = picker
        slctypeTextfield.inputAccessoryView = toolBar
    }
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.maximumDate = NSDate.init() as Date
        datePicker.date = Date()
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton      = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton     = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton    = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        frmDateTextfield.inputAccessoryView      = toolbar
        frmDateTextfield.inputView               = datePicker
        
        toDateTextfield.inputAccessoryView       = toolbar
        toDateTextfield.inputView                = datePicker
    }
    
    @objc func donedatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = DATE_FORMAT
        if isFrmDtPicker {
            frmDateTextfield.text = formatter.string(from: datePicker.date)
        } else {
            toDateTextfield.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func hidePicker() {
        picker.isHidden = true
        datePicker.isHidden = true
        toolBar.isHidden = true
        self.view.endEditing(true)
    }
    
    @IBAction func donePicker() {
        //slctypeTextfield.text = arrSLCtype[picker.selectedRow(inComponent: 0)]
        hidePicker()
        
    }
    
    @IBAction func cancelPicker(){
        hidePicker()
    }
    
    func resignAll() {
        slcidTextfield.resignFirstResponder()
        slctypeTextfield.resignFirstResponder()
        frmDateTextfield.resignFirstResponder()
        toDateTextfield.resignFirstResponder()
    }
    
    @IBAction func searchButtonTapped(sender: UIBarButtonItem) {
        if !isFilterOpen {
            isFilterOpen = true
            consSearchHeight.constant = 174
        } else {
            isFilterOpen = false
            consSearchHeight.constant = 0
        }
        UIView.animate(withDuration: 0.15) {
            self.view.layoutIfNeeded()
        }
        resignAll()
    }
    
    @IBAction func searchButton_Click(sender : UIButton) {
        resignAll()
        pagno = 1
        arrTbldata.removeAll()
        tblData.reloadData()
        totalSlccount = 1
        apicalling()
    }
    
    @IBAction func clearButton_Click(sender : UIButton) {
        resignAll()
        loadData()
        apicalling()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StoryboardIdentifier.DetailsIdentifier {
            if let detailVC = segue.destination as? DetailsVC {
                detailVC.selectedSLC_ID = sender as? String
            }
        }
    }
}

extension ListVC {
    func apicalling() {
        resignAll()
        self.refreshControl.endRefreshing()
        if arrTbldata.count != totalSlccount {
            let parameters = SLClistParams.init(pageno: String(pagno), search: slcidTextfield.text, frmDate: frmDateTextfield.text, toDate: toDateTextfield.text, slcType: slctypeTextfield.text)
            listModel.makeSLCListAPI(formFields: parameters, successHandler:{[unowned self] (listModel) in
                arrTbldata.append(contentsOf: listModel.slclist ?? [])
                totalSlccount = Int(listModel.totalslc ?? "1") ?? 1
                DispatchQueue.main.async {
                    tblData.reloadData()
                }
            }) { (failureStatus) in
                DispatchQueue.main.async {
                    Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
                }
            }
        } else {
            //tblData.reloadData()
        }
    }
}


extension ListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrTbldata.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellList = tableView.dequeueReusableCell(withIdentifier: "CellList") as! CellList
        
        let list = arrTbldata[indexPath.row]
        
        cell.slcidLabel.text = ": \(list.slc_id ?? "")"
        cell.poleidLabel.text = ": \(list.pole_id ?? "")"
        cell.addedonLabel.text = ": \(list.created ?? "")"
        cell.addressLabel.text = ": \(list.address ?? "")"
        cell.slcidHeading.text = "SLC ID"
        cell.poleidHeading.text = "POLE ID"
        cell.addressHeading.text = "ADDRESS"
        cell.addedonHeading.text = "ADDED ON"
        
        if arrTbldata.count-1 == indexPath.row {
            pagno += 1
            apicalling()
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let list = arrTbldata[indexPath.row]
        self.performSegue(withIdentifier: StoryboardIdentifier.DetailsIdentifier, sender: list.id)
    }
}

extension ListVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == slcidTextfield {
            slctypeTextfield.becomeFirstResponder()
        } else if textField == slctypeTextfield {
            frmDateTextfield.becomeFirstResponder()
        } else if textField == frmDateTextfield {
            toDateTextfield.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == slctypeTextfield {
            pickerView()
        } else if textField == frmDateTextfield {
            isFrmDtPicker = true
            showDatePicker()
        } else if textField == toDateTextfield {
            isFrmDtPicker = false
            showDatePicker()
        }
        return true
    }
}

extension ListVC : UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrSLCtype.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrSLCtype[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        slctypeTextfield.text = arrSLCtype[row]
    }
    
}
