//
//  ListViewModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 14/07/21.
//

import Foundation
import UIKit

class ListViewModel {
}

struct SLClistParams {
    var pageno : String?
    var search : String?
    var frmDate : String?
    var toDate : String?
    var slcType : String?
    var slcID  : String?
}

extension ListViewModel {
    func makeSLCListAPI(formFields : SLClistParams, successHandler:@escaping (SLCListmodel) -> Void, failureHandler:@escaping (String) -> Void) {
        
//        lclist/424/1/22.470700/70.057700
        
        let stringUrl = SLCLIST_URL+"/\(String(describing: DBManager.sharedInstance.activeRealmUser?.userID ?? ""))/\(String(describing: formFields.pageno ?? "1"))/\(String(describing: LocationService.sharedInstance.lastLatitude ?? 0.0))/\(String(describing: LocationService.sharedInstance.lastLongitude ?? 0.0))"
        
        print(stringUrl)
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = stringUrl
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "search=\(formFields.search ?? "")&node_type=\("")&from_daterange=\(formFields.frmDate ?? "")&to_daterange=\(formFields.toDate ?? "")&slc_type=\(formFields.slcType ?? "")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            if let dataNew = dicValueString(userDetails[DATA] as? [String : Any] ?? [:])?.data(using: .utf8) {
                                let loginUser = try SLCListmodel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
}
