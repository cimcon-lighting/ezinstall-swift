//
//  DetailsVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 16/07/21.
//

import UIKit
import MapKit
import AlamofireImage

class DetailsCell : UITableViewCell {
    @IBOutlet var titleLabel        : UILabel!
    @IBOutlet var detailLabel       : UILabel!
}

class DetailsVC: UIViewController {
    var detailsModel                = DetailModel()

    @IBOutlet var viewTop           : UIView!
    @IBOutlet var viewBottom        : UIView!
    @IBOutlet var viewMap           : UIView!
    @IBOutlet var viewImage         : UIView!

    @IBOutlet var uidLabel          : UILabel!
    @IBOutlet var slcLabel          : UILabel!
    @IBOutlet var poleLabel         : UILabel!
    @IBOutlet var datelabel         : UILabel!
    @IBOutlet var msgLabel          : UILabel!
    
    @IBOutlet var addressTextView   : UITextView!
    
    @IBOutlet var notesViewbuttton  : UIButton!
    @IBOutlet var editButton        : UIButton!
    @IBOutlet var cameraButton      : UIButton!
    @IBOutlet var mapOkButton       : UIButton!
    @IBOutlet var imageOkButton     : UIButton!
    
    @IBOutlet var tblView           : UITableView!
    
    @IBOutlet var mapView           : MKMapView!
    
    @IBOutlet var imagePoleView     : UIImageView!
    
    var selectedSLC_ID              : String!
    
    var arrTblData                  : [Assets] = []
    
    var slcDetailModel              : SLCDetailModel!
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSLCDataFrmServer()
        self.viewTop.isHidden = true
        self.viewBottom.isHidden = true
        msgLabel.isHidden = true
        msgLabel.text = NO_DATA_FOUND
    }
    
    //MARK:- Button Clicks
    @IBAction func backButton_Click(sender : UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func pinButton_Click(sender : UIBarButtonItem) {
        addView(vw: viewMap)
    }
    @IBAction func editButton_Click(sender : UIButton) {
        
    }
    @IBAction func cameraButton_Click(sender : UIButton) {
        addView(vw: viewImage)
    }
    @IBAction func mapOkButton_Click(sender : UIButton) {
        hideView(vw: viewMap)
    }
    @IBAction func imageOkButton_Click(sender : UIButton) {
        hideView(vw: viewImage)
    }
    
    func addView(vw : UIView){
        self.view.addSubview(vw)
        vw.frame = self.view.bounds
        vw.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            vw.alpha = 1.0
        }
    }
    
    func hideView(vw : UIView) {
        vw.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            vw.alpha = 0.0
        }) { (isDone) in
            vw.removeFromSuperview()
        }
    }
}

extension DetailsVC {
    func getSLCDataFrmServer() {
        detailsModel.makeSLCDetailsAPI(SLC_ID: selectedSLC_ID, successHandler:{[unowned self] (detailModel) in
            DispatchQueue.main.async {
                slcDetailModel = detailModel
                arrTblData = slcDetailModel.assets ?? []
                slcLabel.text = "SLC ID : \(slcDetailModel.slc_id ?? "None")"
                poleLabel.text = "POLE ID : \(slcDetailModel.pole_id ?? "None")"
                let scanlbl = DBManager.sharedInstance.activeRealmUser?.ScanLBL
                uidLabel.text = "\(scanlbl ?? "UID") : \(slcDetailModel.mac_address ?? "None")"
                datelabel.text = slcDetailModel.date_of_installation
                addressTextView.text = slcDetailModel.address
                self.viewTop.isHidden = false
                self.viewBottom.isHidden = false
                if arrTblData.count == 0 {
                    msgLabel.isHidden = false
                }
                if let strurl = slcDetailModel.pole_image_url, strurl != "" {
                    if let url = URL.init(string: strurl) {
                        imagePoleView.af.setImage(withURL: url, placeholderImage: UIImage())
                    }
                }
                tblView.reloadData()
            }
        }) { (failureStatus) in
            DispatchQueue.main.async {
                self.msgLabel.isHidden = false
                Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
            }
        }
    }
}

extension DetailsVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as! DetailsCell
        let assetDetails : Assets = arrTblData[indexPath.row]
        cell.titleLabel.text = assetDetails.attributeName
        cell.detailLabel.text = assetDetails.selected
        return cell
    }
}

extension DetailsVC {
    
}
