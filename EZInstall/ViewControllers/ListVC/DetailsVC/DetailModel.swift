//
//  DetailModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 19/07/21.
//

import Foundation
import UIKit

class DetailModel {
    func makeSLCDetailsAPI(SLC_ID : String, successHandler:@escaping (SLCDetailModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let stringUrl = SLC_DETAILS+"/\(SLC_ID)"
        
        print(stringUrl)
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = stringUrl
        apiRequest.httpMethod   = HTTP_Methods.GET
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            if let dataNew = dicValueString(userDetails[DATA] as? [String : Any] ?? [:])?.data(using: .utf8) {
                                let slcdetail = try SLCDetailModel.init(data: dataNew)
                                successHandler(slcdetail!)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
}
