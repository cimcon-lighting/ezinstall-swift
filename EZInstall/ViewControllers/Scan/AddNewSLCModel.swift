//
//  AddNewSLCModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 22/07/21.
//

import Foundation

class AddNewSLCModel {
    
}

struct AddNewSLCParams {
    var macaddress : String?
    var nodetype : String?
    var slcid    : String?
    var latitude : String?
    var longitude : String?
    var clientid : String?
    var userid : String?
}

extension AddNewSLCModel {
    func makeCheckMacaddressIneternalAPI(formFields : AddNewSLCParams, successHandler:@escaping (NewSLCModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = CHECKMACINTERNAL
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "client_id=\(DBManager.sharedInstance.activeRealmUser?.clientID ?? "")&user_id=\(DBManager.sharedInstance.activeRealmUser?.userID ?? "")&mac_address=\(formFields.macaddress ?? "")&node_type=\(formFields.nodetype ?? "")&source=\("IOS")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        //apiRequest.headers = ["Content-Type":"application/x-www-form-urlencoded",
        ///                    "Accept" : "application/json"]
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            var newaddedData = userDetails
                            newaddedData["UID"] = formFields.macaddress
                            if let dataNew = dicValueString(newaddedData)?.data(using: .utf8) {
                                let loginUser = try NewSLCModel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
    
    
    func makeCheckMacaddressAPI(formFields : AddNewSLCParams, successHandler:@escaping (NewSLCModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = CHECKMACADDRESS
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "client_id=\(DBManager.sharedInstance.activeRealmUser?.clientID ?? "")&user_id=\(DBManager.sharedInstance.activeRealmUser?.userID ?? "")&macaddress=\(formFields.macaddress ?? "")&node_type=\(formFields.nodetype ?? "")&source=\("IOS")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        //apiRequest.headers = ["Content-Type":"application/x-www-form-urlencoded",
        ///                    "Accept" : "application/json"]
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            var newaddedData = userDetails
                            newaddedData["UID"] = formFields.macaddress
                            if let dataNew = dicValueString(newaddedData)?.data(using: .utf8) {
                                let loginUser = try NewSLCModel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
    
    func makeCheckSLCaddressAPI(formFields : AddNewSLCParams, successHandler:@escaping (NewSLCModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = CHECKSLCID
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "client_id=\(DBManager.sharedInstance.activeRealmUser?.clientID ?? "")&user_id=\(DBManager.sharedInstance.activeRealmUser?.userID ?? "")&slcid=\(formFields.slcid ?? "")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            var newaddedData = userDetails
                            newaddedData["UID"] = formFields.macaddress
                            if let dataNew = dicValueString(newaddedData)?.data(using: .utf8) {
                                let loginUser = try NewSLCModel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
    
    
    func makeCheckMacandSCLAPI(formFields : AddNewSLCParams, successHandler:@escaping (NewSLCModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = CHECKMACANDSCLID
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "client_id=\(DBManager.sharedInstance.activeRealmUser?.clientID ?? "")&user_id=\(DBManager.sharedInstance.activeRealmUser?.userID ?? "")&mac_address=\(formFields.macaddress ?? "")&node_type=\(formFields.nodetype ?? "")&source=\("IOS")&slc_id=\(formFields.slcid ?? "")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        //apiRequest.headers = ["Content-Type":"application/x-www-form-urlencoded",
        ///                    "Accept" : "application/json"]
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            var newaddedData = userDetails
                            newaddedData["UID"] = formFields.macaddress
                            newaddedData["slc_id"] = formFields.slcid
                            if let dataNew = dicValueString(newaddedData)?.data(using: .utf8) {
                                let loginUser = try NewSLCModel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
    
    
    
    func makeAddressAPI(formFields : AddNewSLCParams, successHandler:@escaping (NewSLCModel) -> Void, failureHandler:@escaping (String) -> Void) {
        
        let apiRequest          = APIRequest()
        apiRequest.urlString    = ADDRESS
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "client_id=\(formFields.clientid ?? "")&user_id=\(formFields.userid ?? "")&lat=\(formFields.latitude ?? "0.0")&lng=\(formFields.longitude ?? "0.0")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            var newaddedData = userDetails
                            
                            newaddedData["UID"] = formFields.macaddress ?? ""
                            newaddedData["slc_id"] = formFields.slcid ?? ""
                            newaddedData["lat"] = String(formFields.latitude ?? "0.0")
                            newaddedData["lng"] = String(formFields.longitude ?? "0.0")
                            
                            if let dataNew = dicValueString(newaddedData)?.data(using: .utf8) {
                                let address = try NewSLCModel.init(data: dataNew)
                                successHandler(address)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
}
