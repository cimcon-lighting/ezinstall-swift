//
//  LocationVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 08/07/21.
//

import UIKit
import MapKit

class LocationVC: UIViewController {
    
    var addnewslcModel = AddNewSLCModel()
    
    @IBOutlet var mapview   : MKMapView!
    
    @IBOutlet var uidLabel  : UILabel!
    @IBOutlet var slcLabel  : UILabel!
    @IBOutlet var networkstrengthLabel  : UILabel!
    @IBOutlet var accuratesignalLabel   : UILabel!
    @IBOutlet var lattitudeLabel        : UILabel!
    @IBOutlet var longitudelLabel       : UILabel!
    
    var artwork : AnnotationView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationaccuracy()
        mapview.delegate = self
        setupMapview()
    }
    
    func setupMapview() {
        mapview.register(AnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        self.getAddress(lat: String(LocationService.sharedInstance.lastLatitude ?? 0.0), lng: String(LocationService.sharedInstance.lastLongitude ?? 0.0))
        
        mapview.centerToLocation(CLLocation.init(latitude: LocationService.sharedInstance.lastLatitude ?? 0.0, longitude: LocationService.sharedInstance.lastLongitude ?? 0.0))
        if #available(iOS 13.0, *) {
            let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 200000)
            mapview.setCameraZoomRange(zoomRange, animated: true)
        }
    }
    
    func setuppin(title : String, lat : Double, long : Double) {
        mapview.removeAnnotations(self.mapview.annotations)
        self.artwork = AnnotationView.init(title1: title, coordinate1: CLLocationCoordinate2D(latitude: lat, longitude: long))
        mapview.addAnnotation(artwork)
    }
    
    func setupLocationaccuracy() {
        uidLabel.text = "\(DBManager.sharedInstance.activeRealmUser?.ScanLBL ?? "UID") : \(DBManager.sharedInstance.addnewSLC?.udi ?? "")"
        slcLabel.text = "SLC ID : \(DBManager.sharedInstance.addnewSLC?.slcid ?? "")"
        lattitudeLabel.text = String(LocationService.sharedInstance.lastLatitude ?? 0.0)
        longitudelLabel.text = String(LocationService.sharedInstance.lastLongitude ?? 0.0)
        networkstrengthLabel.text = checknetwrokStrength().0
        accuratesignalLabel.text =  checknetwrokStrength().1
    }
    
    func checknetwrokStrength() -> (String, String) {
        var strStrength = ""
        var strAccurate = "0"
        if let location = LocationService.sharedInstance.lastLocation {
            if location.horizontalAccuracy < 0 {
                strStrength = "No Signal"
            } else if location.horizontalAccuracy > 163 {
                strStrength = "Poor"
            } else if location.horizontalAccuracy > 48 {
                strStrength = "Average"
            } else {
                strStrength = "Strong"
            }
            strAccurate = "\(location.horizontalAccuracy) M"
        }
        return (strStrength, strAccurate)
    }
    
    @IBAction func signalStrength_Click (sender : UIButton){
        Alert().showAlert("Satellites", message: "GPS gets a signal from GPS satellites to calculate your position. Here the value is calculated based on the number of satellites connected to and the strength of the signal from these satellites.", okButtonTitle: ALERT_OK_TITLE, okButtonStyle: .default, okButtonHandler: nil, cancelButtonTitle: nil, cancelButtonStyle: .default, cancelButtonHandler: nil, CompletionHandler: nil, View: self)
    }
    
    @IBAction func signalAccuracy_Click (sender : UIButton){
        Alert().showAlert("Accuracy", message: "Accuracy refers to the degree of closeness to the actual position. Lower the number, better the accuracy!", okButtonTitle: ALERT_OK_TITLE, okButtonStyle: .default, okButtonHandler: nil, cancelButtonTitle: nil, cancelButtonStyle: .default, cancelButtonHandler: nil, CompletionHandler: nil, View: self)
    }
    
    @IBAction func backButton_Click(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accessptButton_Click(sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardIdentifier.AddressIdentifier, sender: self)
    }
}


extension LocationVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        var pinView : MKPinAnnotationView? = nil
        
        let reuseId = "pin"
        pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView ?? MKPinAnnotationView()
        
        pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        pinView?.canShowCallout = true
        pinView?.calloutOffset = CGPoint(x: -7, y: -5)
        let button = UIButton.init(frame: CGRect.init(x: 5, y: 5, width: 30, height: 30))
        button.setBackgroundImage(UIImage.init(named: "Pole_Blue"), for: .normal)
        pinView?.isDraggable = true
        pinView?.isSelected = true
        pinView?.animatesDrop = true
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
        switch newState {
        case .ending:
            let droppedAt = view.annotation?.coordinate
            getAddress(lat: String(Double(droppedAt?.latitude ?? 0.0)), lng: String(Double(droppedAt?.longitude ?? 0.0)))
            view.dragState = .none
            
        default: break
        }
    }
}


extension LocationVC {
    func getAddress(lat : String, lng : String) {
        DispatchQueue.main.async {
            let clientID = DBManager.sharedInstance.activeRealmUser?.clientID
            let userID   = DBManager.sharedInstance.activeRealmUser?.userID
            let uid      = DBManager.sharedInstance.addnewSLC?.udi
            let slcid    = DBManager.sharedInstance.addnewSLC?.slcid
            let addnewParams = AddNewSLCParams(macaddress : uid, slcid : slcid, latitude: lat, longitude: lng, clientid: clientID, userid: userID)
            self.addnewslcModel.makeAddressAPI( formFields: addnewParams,successHandler:{ (newslcModel) in
                DispatchQueue.main.async {
                    self.setuppin(title: newslcModel.shortaddress ?? "", lat: Double(newslcModel.latitude ?? "0.0") ?? 0.0, long: Double(newslcModel.longitude ?? "0.0") ?? 0.0)
                    DBManager.sharedInstance.addnewslc(slcObject: newslcModel)
                }
            }) { (failureStatus) in
                DispatchQueue.main.async {
                    Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
                }
            }
        }
    }
}
