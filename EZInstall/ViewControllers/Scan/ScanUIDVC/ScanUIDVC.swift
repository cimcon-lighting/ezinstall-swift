//
//  ScanUIDVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 06/07/21.
//

import UIKit

class ScanUIDVC: UIViewController {

    var addnewslcModel = AddNewSLCModel()
    
    var macaddress      : String!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func manualEntercode_Click(sender : UIBarButtonItem) {
        let title = DBManager.sharedInstance.activeRealmUser?.ScanLBL
        Alert().showAlertwithtextfiled(title: title,
                                       message: "",
                                       inputPlaceholder: title,
                                       okButtonTitle: ALERT_CONFIRM_TITLE,
                                       cancelButtonTitle: ALERT_CANCEL_TITLE,
                                       View: self,
                                       CompletionHandler: nil,
                                       cancelButtonHandler: nil,
                                       okButtonHandler: { (input:String?) in
                                        if input == "" {
                                            Alert().showAlert("Please enter \(title ?? "UID")",
                                                              message: "",
                                                              okButtonTitle: ALERT_OK_TITLE,
                                                              okButtonStyle: .default,
                                                              okButtonHandler: { _ in
                                                                self.manualEntercode_Click(sender: sender)
                                                              }, cancelButtonTitle: nil,
                                                              cancelButtonStyle: .default,
                                                              cancelButtonHandler: nil,
                                                              CompletionHandler: nil,
                                                              View: self)
                                        } else {
                                            self.macaddress = input
                                            self.checkMacInternal()
                                        }
                                    })
    }
}

extension ScanUIDVC {
     func checkMacInternal() {
        let addnewParams = AddNewSLCParams(macaddress: macaddress, nodetype: "")
        addnewslcModel.makeCheckMacaddressIneternalAPI(formFields: addnewParams, successHandler:{ (newslcModel) in
            DispatchQueue.main.async {
                DBManager.sharedInstance.addnewslc(slcObject: newslcModel)
                if newslcModel.macaddresstype == EXTERNAL {
                    self.checkMacaddress()
                } else {
                    //redirect to mapview
                }
            }
        }) { (failureStatus) in
            DispatchQueue.main.async {
                Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
            }
        }
     }
    
    func checkMacaddress() {
        let addnewParams = AddNewSLCParams(macaddress: macaddress, nodetype: "")
        addnewslcModel.makeCheckMacaddressAPI(formFields: addnewParams, successHandler:{ (newslcModel) in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: StoryboardIdentifier.ScanSLCIdentifier, sender: self)
            }
        }) { (failureStatus) in
            DispatchQueue.main.async {
                Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
            }
        }
    }
}
