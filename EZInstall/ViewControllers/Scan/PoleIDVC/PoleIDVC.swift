//
//  PoleIDVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 08/07/21.
//

import UIKit

class PoleIDVC: UIViewController {

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backButton_Click(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButton_Click(sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardIdentifier.NotesIdentifier, sender: self)
    }
}
