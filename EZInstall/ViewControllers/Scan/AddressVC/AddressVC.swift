//
//  AddressVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 08/07/21.
//

import UIKit

class AddressVC: UIViewController {

    @IBOutlet var addressTextview : UITextView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        addressTextview.text = DBManager.sharedInstance.addnewSLC?.address
    }
    
    @IBAction func backButton_Click(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmButton_Click(sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardIdentifier.CameraIdentifier, sender: self)
    }
}
