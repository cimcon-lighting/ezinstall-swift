//
//  ScanSLCVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 06/07/21.
//

import UIKit
import CoreLocation

class ScanSLCVC: UIViewController {

    var addnewslcModel = AddNewSLCModel()
    
    var slcscannedCode      : String!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationService.sharedInstance.delegate = self
        LocationService.sharedInstance.startUpdatingLocation()
    }
    
    func locationError() {
        Alert().showAlert(LOCATION_ERROR, message: "", okButtonTitle: "Settings", okButtonStyle: .default, okButtonHandler: { _ in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }, cancelButtonTitle: ALERT_CANCEL_TITLE, cancelButtonStyle: .default, cancelButtonHandler: nil, CompletionHandler: nil, View: (appDelegate.window?.rootViewController)!)
    }
    
    
    @IBAction func backButton_Click(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButton_Click(sender : UIButton) {
       
    }
    
    @IBAction func manualEntercode_Click(sender : UIBarButtonItem) {
        let title = "SLC ID"
        Alert().showAlertwithtextfiled(title: title,
                                       message: "",
                                       inputPlaceholder: title,
                                       okButtonTitle: ALERT_CONFIRM_TITLE,
                                       cancelButtonTitle: ALERT_CANCEL_TITLE,
                                       View: self,
                                       CompletionHandler: nil,
                                       cancelButtonHandler: nil,
                                       okButtonHandler: { (input:String?) in
                                        if input == "" {
                                            Alert().showAlert("Please enter \(title)",
                                                              message: "",
                                                              okButtonTitle: ALERT_OK_TITLE,
                                                              okButtonStyle: .default,
                                                              okButtonHandler: { _ in
                                                                self.manualEntercode_Click(sender: sender)
                                                              }, cancelButtonTitle: nil,
                                                              cancelButtonStyle: .default,
                                                              cancelButtonHandler: nil,
                                                              CompletionHandler: nil,
                                                              View: self)
                                        } else {
                                            self.slcscannedCode = input
                                            if LocationService.sharedInstance.lastLatitude == 0.0 {
                                                self.locationError()
                                            } else {
                                                self.checkslcid()
                                            }
                                        }
                                    })
    }
    
}

extension ScanSLCVC {
     func checkslcid() {
        let addnewParams = AddNewSLCParams(slcid: slcscannedCode)
        addnewslcModel.makeCheckSLCaddressAPI(formFields: addnewParams, successHandler:{ (newslcModel) in
            DispatchQueue.main.async {
                self.checkmacandslcid()
            }
        }) { (failureStatus) in
            DispatchQueue.main.async {
                Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
            }
        }
     }
    
    func checkmacandslcid() {
        let addnewParams = AddNewSLCParams(macaddress: DBManager.sharedInstance.addnewSLC?.udi,slcid: slcscannedCode)
        addnewslcModel.makeCheckMacandSCLAPI(formFields: addnewParams, successHandler:{ (newslcModel) in
            DispatchQueue.main.async {
                DBManager.sharedInstance.addnewslc(slcObject: newslcModel)
                self.performSegue(withIdentifier: StoryboardIdentifier.LocationIdentifier, sender: self)
            }
        }) { (failureStatus) in
            DispatchQueue.main.async {
                Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
            }
        }
    }
}

extension ScanSLCVC : LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        print("\(currentLocation)")
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        self.locationError()
    }
}
