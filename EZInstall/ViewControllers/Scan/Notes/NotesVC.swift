//
//  NotesVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 08/07/21.
//

import UIKit

class NotesCell : UITableViewCell {
    @IBOutlet var titleLabel        : UILabel!
    @IBOutlet var valueTextfield    : UITextField!
    @IBOutlet var checkButton       : UIButton!
}

class NotesVC: UIViewController {

    var arrTitle                    : [String] = []
    
    @IBOutlet var tblview           : UITableView!
    
    @IBOutlet var txtView           : UITextView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backButton_Click(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipButton_Click(sender: UIButton) {
        self.performSegue(withIdentifier: StoryboardIdentifier.PoleIDIdentifier, sender: self)
    }
    
    @IBAction func checkButton_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
    }
}


extension NotesVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10//arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotesCell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as! NotesCell
        cell.checkButton.addTarget(self, action: #selector(checkButton_Click(sender:)), for: .touchUpInside)
        return cell
    }
}
