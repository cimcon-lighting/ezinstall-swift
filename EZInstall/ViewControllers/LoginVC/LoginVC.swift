//
//  LoginVC.swift
//  EZInstall
//
//  Created by Akash Mehta on 05/07/21.
//

import UIKit

class LoginVC: UIViewController {

    var loginModel                  = LoginViewModel()
    
    @IBOutlet var passwordTextfield     : UITextField!
    @IBOutlet var usernameTextfield     : UITextField!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationService.sharedInstance.startUpdatingLocation()
    }
    
    //MARK:- Button Clicks
    @IBAction func backButton_Click(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eyeButton_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextfield.isSecureTextEntry = sender.isSelected
        passwordTextfield.resignFirstResponder()
    }
    
    @IBAction func rememberButton_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func loginButton_Click(sender : UIButton){
        
        usernameTextfield.resignFirstResponder()
        passwordTextfield.resignFirstResponder()
        
        let clientID = DBManager.sharedInstance.activeRealmUser?.clientID
        let userid = DBManager.sharedInstance.activeRealmUser?.userID
        
        let formFields = FormValidationModel.init(email: usernameTextfield.text, pass: passwordTextfield.text, clientid: clientID, userid: userid)
        if let message = loginModel.validateForm(formFields: formFields) {
            Alert().showAlert(ALERT_TITLE, message: message, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
        } else {
            loginModel.makeLoginAPI(formFields: formFields, successHandler: {[unowned self] (securityModel) in
                //SAVING DATA TO DB
                let sModel = securityModel
                
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(AllStoryboards.tabbarViewcontroller(), animated: true)
                    DBManager.sharedInstance.addUserSettings(userObject: sModel)
                }
                
                
            }) { (failureStatus) in
                DispatchQueue.main.async {
                    Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
                }
            }
        }
    }
}
