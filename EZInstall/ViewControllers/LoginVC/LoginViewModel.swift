//
//  LoginViewModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 13/07/21.
//

import Foundation
import UIKit

class LoginViewModel {
}

class FormValidationModel  {
    //MARK:- VARIABLE
    var securitycode    : String? = nil
    var emailid         : String? = nil
    var password        : String? = nil
    var language        : String? = nil
    var clientid        : String? = nil
    var userid          : String? = nil
    
    init(scode: String? = nil,
         email: String? = nil,
         pass: String? = nil,
         clientid: String? = nil,
         userid: String? = nil) {
        self.securitycode   = scode
        self.emailid        = email
        self.password       = pass
        self.clientid       = clientid
        self.userid         = userid
    }
}

extension LoginViewModel {
    func validateForm(formFields: FormValidationModel) -> String? {
        
        if let displayName = formFields.securitycode, displayName.isEmpty {
            return ValidationError.ErrorMessages.msgEmptySecuritycode
        }
        
        if let emailId = formFields.emailid {
            if emailId.isEmpty {
                return ValidationError.ErrorMessages.msgEmptyUsername
            }
        }
        
        if let password = formFields.password, password.isEmpty {
            return ValidationError.ErrorMessages.msgEmptyPassword
        }
        
        return nil
    }
}


extension LoginViewModel {
    func makeSecurityAPI(formFields : FormValidationModel, successHandler:@escaping (SecurityModel) -> Void, failureHandler:@escaping (String) -> Void) {
        let apiRequest          = APIRequest()
        apiRequest.urlString    = SECURITY_URL
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let parameters = "code=\(formFields.securitycode ?? "")&language=\("en")&source=\("IOS")&udid=\(UIDevice.current.identifierForVendor!.uuidString)".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            if let dataNew = dicValueString(userDetails)?.data(using: .utf8) {
                                let loginUser = try SecurityModel.init(data: dataNew)
                                successHandler(loginUser)
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
    
    
    func makeLoginAPI(formFields : FormValidationModel, successHandler:@escaping (SecurityModel) -> Void, failureHandler:@escaping (String) -> Void) {
        let apiRequest          = APIRequest()
        apiRequest.urlString    = LOGIN_URL
        apiRequest.httpMethod   = HTTP_Methods.POST
        
        let latitude  = LocationService.sharedInstance.lastLatitude
        let longitude = LocationService.sharedInstance.lastLongitude
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let parameters = "client_id=\(formFields.clientid ?? "")&userid=\(formFields.userid ?? "")&username=\(formFields.emailid ?? "")&password=\(formFields.password ?? "")&long=\(String(describing: longitude ?? 0.0))&lat=\(String(describing: latitude ?? 0.0))&Version=\(String(describing: appVersion ?? "1.0"))&source=\("IOS")".data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        apiRequest.httpBody = parameters
        
        NewtworkManager.shared.makeAPICalls(apiRequest: apiRequest, isLoaderShow: true) { (data, response, error) in
            if response != nil {
                guard error == nil else {
                    failureHandler("Network Issue.")
                    return
                }
                do{
                    if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        if let userDetails = json {
                            print("RESPONSE : \(userDetails)")
                            //                            if let dataNew = dicValueString(userDetails)?.data(using: .utf8) {
                            var loginUser : SecurityModel!
                            DispatchQueue.main.async {
                                let clientID = DBManager.sharedInstance.activeRealmUser?.clientID
                                let userid = DBManager.sharedInstance.activeRealmUser?.userID
                                let slc_view = DBManager.sharedInstance.activeRealmUser?.slc_view
                                let slc_edit = DBManager.sharedInstance.activeRealmUser?.slc_edit
                                let pole_id  = DBManager.sharedInstance.activeRealmUser?.pole_id
                                let pole_asset = DBManager.sharedInstance.activeRealmUser?.pole_asset
                                let pole_img = DBManager.sharedInstance.activeRealmUser?.pole_img
                                let scanlbl = DBManager.sharedInstance.activeRealmUser?.ScanLBL
                                
                                let settings = Setting.init(client_slc_list_view: slc_view, client_slc_edit_view: slc_edit, client_slc_pole_image_view: pole_img, client_slc_pole_assets_view: pole_asset, client_slc_pole_id: pole_id)
                                
                                do{
                                    loginUser = try SecurityModel.init(status: "", msg: "", clientID: clientID, userid: userid, setting: settings, token: userDetails[TOKEN] as? String, scanlbl: scanlbl)
                                    successHandler(loginUser)
                                } catch {
                                    failureHandler(OOPS_MSG)
                                }
                            }
                        } else {
                            failureHandler(OOPS_MSG)
                        }
                    } else {
                        failureHandler(OOPS_MSG)
                    }
                } catch {
                    failureHandler(OOPS_MSG)
                }
            } else {
                failureHandler(NetworkError.msgNetworkError)
            }
        }
    }
}
