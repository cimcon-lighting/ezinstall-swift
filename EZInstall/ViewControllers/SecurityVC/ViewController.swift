//
//  ViewController.swift
//  EZInstall
//
//  Created by Akash Mehta on 05/07/21.
//

import UIKit

class ViewController: UIViewController {
    
    var loginModel                  = LoginViewModel()
    
    @IBOutlet var securityTextfield : UITextField!
    
    @IBOutlet var submitButton      : UIButton!
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

//    MARK:- Button Clicks
    @IBAction func submitButton_Click(sender : UIButton){
        
        DBManager.sharedInstance.deleteAllFromDatabase()
        
        securityTextfield.resignFirstResponder()
        
        let formFields = FormValidationModel.init(scode: securityTextfield.text)
        if let message = loginModel.validateForm(formFields: formFields) {
            Alert().showAlert(ALERT_TITLE, message: message, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
        } else {
            loginModel.makeSecurityAPI(formFields: formFields, successHandler: {[unowned self] (securityModel) in
                
                //SAVING DATA TO DB
                let sModel = securityModel
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: StoryboardIdentifier.LoginIdentifier, sender: self)
                    DBManager.sharedInstance.addUserSettings(userObject: sModel)
                }
                
                
            }) { (failureStatus) in
                DispatchQueue.main.async {
                    Alert().showAlert(ALERT_TITLE, message: failureStatus, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: self)
                }
            }
        }
    } /*{
        
        
    }*/
    @IBAction func eyeButton_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        securityTextfield.isSecureTextEntry = sender.isSelected
        securityTextfield.resignFirstResponder()
    }
    
}

extension ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
