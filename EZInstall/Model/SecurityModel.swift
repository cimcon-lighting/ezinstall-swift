//
//  SecurityModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 13/07/21.
//

import Foundation

class SecurityModel: Codable {
    var status          : String?
    var msg             : String?
    var clientID        : String?
    var userid          : String?
    var setting         : Setting?
    var token           : String?
    var scanlbl         : String?

    enum CodingKeys: String, CodingKey {
        case status     = "status"
        case msg        = "msg"
        case clientID   = "ClientID"
        case userid     = "userid"
        case setting    = "setting"
        case token      = "token"
        case scanlbl    = "ScanLBL"
    }
    
    init(status: String?,
         msg: String?,
         clientID: String?,
         userid: String?,
         setting : Setting?,
         token : String?,
         scanlbl : String?) {
        self.status     = status
        self.msg        = msg
        self.clientID   = clientID
        self.userid     = userid
        self.setting    = setting
        self.token      = token
        self.scanlbl    = scanlbl
    }
}

class Setting: Codable {
    var client_slc_list_view    : String?
    var client_slc_edit_view    : String?
    var client_slc_pole_image_view : String?
    var client_slc_pole_assets_view : String?
    var client_slc_pole_id      : String?
    
    enum CodingKeys: String, CodingKey {
        case client_slc_list_view   = "client_slc_list_view"
        case client_slc_edit_view   = "client_slc_edit_view"
        case client_slc_pole_image_view     = "client_slc_pole_image_view"
        case client_slc_pole_assets_view    = "client_slc_pole_assets_view"
        case client_slc_pole_id     = "client_slc_pole_id"
    }
    
    init(client_slc_list_view: String?,
         client_slc_edit_view: String?,
         client_slc_pole_image_view: String?,
         client_slc_pole_assets_view: String?,
         client_slc_pole_id: String?) {
        self.client_slc_list_view = client_slc_list_view
        self.client_slc_edit_view = client_slc_edit_view
        self.client_slc_pole_image_view = client_slc_pole_image_view
        self.client_slc_pole_assets_view = client_slc_pole_assets_view
        self.client_slc_pole_id = client_slc_pole_id
    }
}

extension SecurityModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(SecurityModel.self, from: data)
        self.init(status    : me.status,
                  msg       : me.msg,
                  clientID  : me.clientID,
                  userid    : me.userid,
                  setting   : me.setting,
                  token     : me.token,
                  scanlbl   : me.scanlbl)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status          : String?? = nil,
        msg             : String?? = nil,
        userid          : String?? = nil,
        clientID        : String?? = nil,
        setting         : Setting?? = nil,
        token           : String?? = nil,
        scanlbl         : String?? = nil
        ) -> SecurityModel {
        return SecurityModel(status: status ?? self.status,
                         msg: msg ?? self.msg,
                         clientID: clientID ?? self.clientID,
                         userid: userid ?? self.userid,
                         setting : setting ?? self.setting,
                         token : token ?? self.token,
                         scanlbl : scanlbl ?? self.scanlbl)
    }
}
