//
//  NewSLCModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 22/07/21.
//

import Foundation

class NewSLCModel: Codable {
    var macaddresstype: String?
    var slcid: String?
    var uid : String?
    var latitude : String?
    var longitude : String?
    var address : String?
    var shortaddress : String?
    
    enum CodingKeys: String, CodingKey {
        case macaddresstype = "mac_address_type"
        case slcid = "slc_id"
        case uid = "UID"
        case latitude = "lat"
        case longitude = "lng"
        case address = "address"
        case shortaddress = "shortaddress"
    }
    
    init(macaddresstype: String?,
         slcid: String?,
         uid : String?,
         lat : String?,
         lng : String?,
         address : String?,
         shortaddress : String?) {
        self.macaddresstype = macaddresstype
        self.slcid = slcid
        self.uid = uid
        self.latitude = lat
        self.longitude = lng
        self.shortaddress = shortaddress
        self.address = address
    }
}

extension NewSLCModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(NewSLCModel.self, from: data)
        self.init(macaddresstype: me.macaddresstype,
                  slcid: me.slcid,
                  uid: me.uid,
                  lat : me.latitude,
                  lng : me.longitude,
                  address : me.address,
                  shortaddress : me.shortaddress)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        macaddresstype: String?? = nil,
        slcid: String?? = nil,
        uid: String?? = nil,
        lat : String?? = nil,
        lng : String?? = nil,
        address : String?? = nil,
        shortaddress : String?? = nil
        ) -> NewSLCModel {
        return NewSLCModel(macaddresstype: macaddresstype ?? self.macaddresstype,
                           slcid: slcid ?? self.slcid,
                           uid: uid ?? self.uid,
                           lat: lat ?? self.latitude,
                           lng: lng ?? self.longitude,
                           address : address ?? self.address,
                           shortaddress : shortaddress ?? self.shortaddress)
    }
}
