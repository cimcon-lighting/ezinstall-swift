//
//  SLCListModel.swift
//  EZInstall
//
//  Created by Akash Mehta on 14/07/21.
//

import Foundation

class SLCListmodel: Codable {
    var slclist : [List]?
    var totalslc : String?
    
    enum CodingKeys: String, CodingKey {
        case slclist = "List"
        case totalslc = "tot_no_of_records"
    }
    
    init(slclist: [List]?, totalslc: String?) {
        self.slclist = slclist
        self.totalslc = totalslc
    }
}

class List : Codable {
    var id : String?
    var mac_address : String?
    var slc_id : String?
    var pole_id : String?
    var lat : String?
    var long : String?
    var internalmacaddress : String?
    var internalslcid : String?
    var created : String?
    var nodetype : String?
    var address : String?
    var macaddresstype : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case mac_address = "mac_address"
        case slc_id = "slc_id"
        case pole_id = "pole_id"
        case lat = "lat"
        case long = "long"
        case internalmacaddress = "internal_mac_address"
        case internalslcid = "internal_slc_id"
        case created = "created"
        case nodetype = "node_type"
        case address = "address"
        case macaddresstype = "mac_address_type"
    }
    
    init(id : String?,
         mac : String?,
         slc_id : String?,
         pole_id : String?,
         lat : String?,
         long : String?,
         internalMac : String?,
         internalslc : String?,
         created : String?,
         nodetype : String?,
         address : String?,
         mactype : String?
    ) {
        self.id = id
        self.mac_address = mac
        self.slc_id = slc_id
        self.pole_id = pole_id
        self.lat = lat
        self.long = long
        self.internalmacaddress = internalMac
        self.internalslcid = internalslc
        self.created = created
        self.nodetype = nodetype
        self.address = address
        self.macaddresstype = mactype
    }
}



extension SLCListmodel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(SLCListmodel.self, from: data)
        self.init(slclist: me.slclist,
                       totalslc: me.totalslc)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        slclist : [List]?? = nil,
        totalslc : String?? = nil
        ) -> SLCListmodel {
        return SLCListmodel(slclist: slclist ?? self.slclist,
                            totalslc: totalslc ?? self.totalslc)
    }
}
