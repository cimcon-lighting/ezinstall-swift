//
//  GeneralModel.swift
//  Vizsafe
//
//  Created by Akash Mehta on 03/07/21.
//

import Foundation

class GeneralModel: Codable {
    let httpCode: Int?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case httpCode = "httpCode"
        case message = "message"
    }
    
    init(httpCode: Int?, message: String?) {
        self.httpCode = httpCode
        self.message = message
    }
}

// MARK: Convenience initializers and mutators

extension GeneralModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(GeneralModel.self, from: data)
        self.init(httpCode: me.httpCode, message: me.message)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        httpCode: Int?? = nil,
        message: String?? = nil
        ) -> GeneralModel {
        return GeneralModel(
            httpCode: httpCode ?? self.httpCode,
            message: message ?? self.message
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
