

import Foundation
class SLCDetailModel : Codable {
	var iD : String?
	var client_id : String?
	var user_id : String?
	var mac_address_type : String?
	var mac_address : String?
	var slc_id : String?
	var lat : String?
	var lng : String?
	var address : String?
	var measurement_units : String?
	var pole_id : String?
	var pole_image : String?
	var pole_image_url : String?
	var date_of_installation : String?
	var notes : String?
	var node_type : String?
	var language : String?
    var pole_options : [Pole_options]?
	var assets : [Assets]?

	enum CodingKeys: String, CodingKey {
		case iD = "ID"
		case client_id = "client_id"
		case user_id = "user_id"
		case mac_address_type = "mac_address_type"
		case mac_address = "mac_address"
		case slc_id = "slc_id"
		case lat = "lat"
		case lng = "lng"
		case address = "address"
		case measurement_units = "measurement_units"
		case pole_id = "pole_id"
		case pole_image = "pole_image"
		case pole_image_url = "pole_image_url"
		case date_of_installation = "date_of_installation"
		case notes = "notes"
		case node_type = "node_type"
		case language = "language"
		case pole_options = "pole_options"
		case assets = "Assets"
	}

    init(iD : String?,
         client_id : String?,
         user_id : String?,
         mac_address_type : String?,
         mac_address : String?,
         slc_id : String?,
         lat : String?,
         lng : String?,
         address : String?,
         measurement_units : String?,
         pole_id : String?,
         pole_image : String?,
         pole_image_url : String?,
         date_of_installation : String?,
         notes : String?,
         node_type : String?,
         language : String?,
         pole_options : [Pole_options]?,
         assets : [Assets]?
         )  {
        self.iD = iD
        self.client_id = client_id
        self.user_id = user_id
        self.mac_address_type = mac_address_type
        self.mac_address = mac_address
        self.slc_id = slc_id
        self.lat = lat
        self.lng = lng
        self.address = address
        self.measurement_units = measurement_units
        self.pole_id = pole_id
        self.pole_image = pole_image
        self.pole_image_url = pole_image_url
        self.date_of_installation = date_of_installation
        self.notes = notes
        self.node_type = node_type
        self.language = language
        self.pole_options = pole_options
        self.assets = assets
	}
}

extension SLCDetailModel {
    convenience init?(data: Data) throws {
        let me = try newJSONDecoder().decode(SLCDetailModel.self, from: data)
         self.init(iD : me.iD,
                  client_id : me.client_id,
                  user_id : me.user_id,
                  mac_address_type : me.mac_address_type,
                  mac_address : me.mac_address,
                  slc_id : me.slc_id,
                  lat : me.lat,
                  lng : me.lng,
                  address : me.address,
                  measurement_units : me.measurement_units,
                  pole_id : me.pole_id,
                  pole_image : me.pole_image,
                  pole_image_url : me.pole_image_url,
                  date_of_installation : me.date_of_installation,
                  notes : me.notes,
                  node_type : me.node_type,
                  language : me.language,
                  pole_options : me.pole_options,
                  assets : me.assets)
    }
    
    func with(iD : String?? = nil,
              client_id : String?? = nil,
              user_id : String?? = nil,
              mac_address_type : String?? = nil,
              mac_address : String?? = nil,
              slc_id : String?? = nil,
              lat : String?? = nil,
              lng : String?? = nil,
              address : String?? = nil,
              measurement_units : String?? = nil,
              pole_id : String?? = nil,
              pole_image : String?? = nil,
              pole_image_url : String?? = nil,
              date_of_installation : String?? = nil,
              notes : String?? = nil,
              node_type : String?? = nil,
              language : String?? = nil,
              pole_options : [Pole_options]?? = [],
              assets : [Assets]?? = []
    ) -> SLCDetailModel {
        return SLCDetailModel(iD: iD ?? self.iD,
                              client_id: client_id ?? self.client_id,
                              user_id: user_id ?? self.user_id,
                              mac_address_type: mac_address ?? self.mac_address_type,
                              mac_address: mac_address ?? self.mac_address,
                              slc_id: slc_id ?? self.slc_id,
                              lat: lat ?? self.lat,
                              lng: lng ?? self.lng,
                              address: address ?? self.address,
                              measurement_units: measurement_units ?? self.measurement_units,
                              pole_id: pole_id ?? self.pole_id,
                              pole_image: pole_image ?? self.pole_image,
                              pole_image_url: pole_image_url ?? self.pole_image_url,
                              date_of_installation: date_of_installation ?? self.date_of_installation,
                              notes: notes ?? self.notes,
                              node_type: node_type ?? self.node_type,
                              language: language ?? self.language,
                              pole_options: pole_options ?? self.pole_options,
                              assets: assets ?? self.assets)
    }
    
}


class Assets : Codable {
    var attrKey : String?
    var assetName : String?
    var note : String?
    var btnText : String?
    var attributeName : String?
    var values : [String]?
    var selected : String?
    var isRequire : String?
    var ispicklist : String?
    var type : String?

    enum CodingKeys: String, CodingKey {
        case attrKey = "AttrKey"
        case assetName = "AssetName"
        case note = "Note"
        case btnText = "BtnText"
        case attributeName = "AttributeName"
        case values = "Values"
        case selected = "Selected"
        case isRequire = "isRequire"
        case ispicklist = "ispicklist"
        case type = "type"
    }

    init(attrKey : String?,
         assetName : String?,
         note : String?,
         btnText : String?,
         attributeName : String?,
         values : [String]?,
         selected : String?,
         isRequire : String?,
         ispicklist : String?,
         type : String?
         )  {
        self.attrKey = attrKey
        self.assetName = assetName
        self.note =  note
        self.btnText =  btnText
        self.attributeName =  attributeName
        self.values = values
        self.selected = selected
        self.isRequire = isRequire
        self.ispicklist = ispicklist
        self.type = type
    }

}


class Pole_options : Codable {
    var key : String?
    var value : String?
    var camera : String?
    var camera_text : String?

    enum CodingKeys: String, CodingKey {
        case key = "key"
        case value = "value"
        case camera = "Camera"
        case camera_text = "Camera_text"
    }

    init(key : String?,
         value : String?,
         camera : String?,
         camera_text : String?)  {
        self.key = key
        self.value = value
        self.camera = camera
        self.camera_text = camera_text
    }

}
