//
//  DBManager.swift
//  Tindav
//
//  Created by Ghouse Basha Shaik on 22/09/18.
//  Copyright © 2018 Tindav. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import UserNotifications

class DBManager {
    private var   database:Realm
    static let   sharedInstance = DBManager()
    var activeRealmUser : RealmUser?
    var addnewSLC       : RealmAddnewSLC?
    private init() {
        database = try! Realm()
        print("REALM DB FILE PATH : \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
    }
}

extension DBManager {
//ADDING USER TO REALM
    func addUserSettings(userObject: SecurityModel) {
        let realmUser = RealmUser()
        realmUser.clientID      =    userObject.clientID ?? ""
        realmUser.userID        =    userObject.userid ?? ""
        realmUser.pole_id       =    userObject.setting?.client_slc_pole_id ?? ""
        realmUser.pole_img      =    userObject.setting?.client_slc_pole_image_view ?? ""
        realmUser.pole_asset    =    userObject.setting?.client_slc_pole_assets_view ?? ""
        realmUser.slc_view      =    userObject.setting?.client_slc_list_view ?? ""
        realmUser.slc_edit      =    userObject.setting?.client_slc_edit_view ?? ""
        realmUser.token         =    userObject.token ?? ""
        realmUser.ScanLBL       =    userObject.scanlbl ?? ""
        
        try! database.write {
            database.add(realmUser)
            activeRealmUser = realmUser
        }
    }
    
    func addnewslc(slcObject : NewSLCModel) {
        let addnewDataSLC = RealmAddnewSLC()
        addnewDataSLC.slcid = slcObject.slcid ?? ""
        addnewDataSLC.udi   = slcObject.uid ?? ""
        addnewDataSLC.lat   = slcObject.latitude ?? ""
        addnewDataSLC.long  = slcObject.longitude ?? ""
        addnewDataSLC.address  = slcObject.address ?? ""
        
        try! database.write {
            database.add(addnewDataSLC)
            addnewSLC = addnewDataSLC
        }
    }
}

//DELETE FUNCITONS
extension DBManager {
    func deleteAllFromDatabase()  {
        try!   database.write {
            database.deleteAll()
        }
    }
    
    func deletenewSLC() {
        let results = database.objects(RealmAddnewSLC.self)
        for result in results {
            try! database.write {
                database.delete(result)
            }
        }
    }
}


//FETCH FUNCTIONS
extension DBManager {
    func returnUserDetails() -> SecurityModel? {
        let results = database.objects(RealmUser.self)
        if let result = results.first {
            let user = SecurityModel.init(status: "",
                                          msg : "",
                                          clientID: result.clientID,
                                          userid: result.userID,
                                          setting: Setting.init(client_slc_list_view: result.slc_view,
                                                                client_slc_edit_view: result.slc_edit,
                                                                client_slc_pole_image_view: result.pole_img,
                                                                client_slc_pole_assets_view: result.pole_asset,
                                                                client_slc_pole_id: result.pole_id),
                                          token: result.token,
                                          scanlbl: result.ScanLBL)
            return user
        }
        return nil
    }
}

//HELPER FUNCTIONS
extension DBManager {
    
    func activeUser() {
        let results = database.objects(RealmUser.self)
        if let result = results.first {
           activeRealmUser = result
        }
    }
    
    func addnewslc() {
        let results = database.objects(RealmAddnewSLC.self)
        if let result = results.first {
           addnewSLC = result
        }
    }
    
    func userExist() -> Bool {
        let results = database.objects(RealmUser.self)
        if let _ = results.first {
            return true
        }
    return false
    }
}
