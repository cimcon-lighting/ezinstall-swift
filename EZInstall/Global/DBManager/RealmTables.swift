//
//  RealmTables.swift
//  Tindav
//
//  Created by Ghouse Basha Shaik on 22/09/18.
//  Copyright © 2018 Tindav. All rights reserved.
//

import Foundation
import RealmSwift

class RealmUser: Object {
    @objc dynamic var  clientID          = ""
    @objc dynamic var  userID            = ""
    @objc dynamic var  slc_view          = ""
    @objc dynamic var  slc_edit          = ""
    @objc dynamic var  pole_img          = ""
    @objc dynamic var  pole_asset        = ""
    @objc dynamic var  pole_id           = ""
    @objc dynamic var  token             = ""
    @objc dynamic var  ScanLBL           = ""
}


class RealmAddnewSLC : Object {
    @objc dynamic var  udi              = ""
    @objc dynamic var  slcid            = ""
    @objc dynamic var  lat              = ""
    @objc dynamic var  long             = ""
    @objc dynamic var  address          = ""
    
}
