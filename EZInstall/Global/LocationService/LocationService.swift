//
//  LocationService.swift
//  EZInstall
//
//  Created by Akash Mehta on 14/07/21.
//

import Foundation
import CoreLocation
import UIKit

protocol LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationService()
    
    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var lastLatitude: CLLocationDegrees?
    var lastLongitude : CLLocationDegrees?
    var delegate: LocationServiceDelegate?
    
    override init() {
        super.init()
        
        self.locationManager = CLLocationManager()
        guard let locationManager = self.locationManager else {
            return
        }
        
        if CLLocationManager.authorizationStatus() == .denied {
            /*Alert().showAlert(LOCATION_ERROR, message: "", okButtonTitle: "Settings", okButtonStyle: .default, okButtonHandler: { _ in
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            }, cancelButtonTitle: ALERT_CANCEL_TITLE, cancelButtonStyle: .default, cancelButtonHandler: nil, CompletionHandler: nil, View: (appDelegate.window?.rootViewController)!)*/
        } else if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 100
        locationManager.delegate = self
    }
    
    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager?.stopUpdatingLocation()
    }
    
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        
        // singleton for get last location
        self.lastLocation = location
        
        print("LOCATION : \(self.lastLocation?.coordinate)")
        
        self.lastLatitude = self.lastLocation?.coordinate.latitude
        self.lastLongitude = self.lastLocation?.coordinate.longitude
        
        // use for real time update location
        updateLocation(currentLocation: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        // do on error
        updateLocationDidFailWithError(error: error as NSError)
    }
    
    // Private function
    private func updateLocation(currentLocation: CLLocation){
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocation(currentLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationDidFailWithError(error: error)
    }
}


