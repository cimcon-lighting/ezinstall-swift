//
//  Date.swift
//  EZInstall
//
//  Created by Akash Mehta on 14/07/21.
//

import Foundation

extension Date {
    func isGreaterThanOrEqualToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanOrEqualToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded as Date
    }
    
    func addHours(hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded as Date
    }
    
    func add(workingDays: Int, withHolidaysOn holidays: [Date] = []) -> Date{
        let currentDayIndex : Int = Calendar.current.component(.weekdayOrdinal, from: self)
        let restOfTheWorkingWeek : Int = (5-currentDayIndex)
        let daysTobeAdded : Int = workingDays - restOfTheWorkingWeek
        let numberOfWeekendDays : Int = (daysTobeAdded/5)*2
        var daysToAdd : Int =  restOfTheWorkingWeek + 2 + daysTobeAdded + numberOfWeekendDays
        if workingDays % 5 == 0 {
            //We take out the last 2 weekend days
            daysToAdd -= 2
        }
        
        //Add holidays
        var calculatedDate : Date = Calendar.current.date(byAdding: .day, value: daysToAdd, to: self) ?? self
        for holiday in holidays {
            guard holiday.compare(self) == .orderedDescending && holiday.compare(calculatedDate) == .orderedAscending else{ continue }
            calculatedDate = Calendar.current.date(byAdding: .day, value: 1, to: calculatedDate) ?? calculatedDate
        }
        return calculatedDate
    }
    
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
