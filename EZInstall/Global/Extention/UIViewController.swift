//
//  UIViewController.swift
//  Vizsafe
//
//  Created by Akash Mehta on 29/06/21.
//

import Foundation
import UIKit

extension UIViewController {
    func backButton(withTitle: String? = nil) {
        self.navigationController?.isNavigationBarHidden = false
        var title = " "
        if let value = withTitle {
            title = value
        }
        self.navigationController?.navigationBar.topItem?.title = title
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func leftButton(imgLeftbarButton: String) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.init(hexString: ColorCodes.BUTTON_BLUE)
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: imgLeftbarButton), style: UIBarButtonItem.Style.done, target:self, action:#selector(leftButtonClick))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func rightButton(imgLeftbarButton: String) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.init(hexString: ColorCodes.BUTTON_BLUE)
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: imgLeftbarButton), style: UIBarButtonItem.Style.done, target:self, action:#selector(rightButtonClick))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func leftButtonClick() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func rightButtonClick() {
       
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
            execute: closure)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

