//
//  String.swift
//  iRTU6000
//
//  Created by Aakash Mehta on 29/05/21.
//

import Foundation
import UIKit

extension String {
    var isEmailFormatValid: Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
}
