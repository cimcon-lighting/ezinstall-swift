//
//  StoryboardHelper.swift
//

import Foundation
import UIKit

struct AllStoryboards {
   /* struct Login_Storyboard {
        static func login_viewcontroller() -> LoginViewController {
            let storyboard = UIStoryboard(name: "Login", bundle: Bundle(nil)
            let viewController = storyboard.instantiateViewController(withIdentifier:"login_sb_id") as! LoginViewController
            return viewController
        }
    }*/
    
    static func tabbarViewcontroller() -> UIViewController {
        let storyboard = UIStoryboard(name: "Tabbar", bundle: nil)
        let viewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "TabbarIdentifier") as! TabbarVC
        return viewController
    }
    
    static func loginViewcontroller() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationviewController: UINavigationController = storyboard.instantiateViewController(withIdentifier: "Mainnavigation") as! UINavigationController
        return navigationviewController
       /* if (viewController != nil) {
            let navigationController = UINavigationController(rootViewController: viewController!)
            return navigationController
        }
        return UINavigationController()*/
    }
    
}
