//
//  CommonFunctions.swift
//  Tindav
//
//  Created by Ghouse Basha Shaik on 18/07/18.
//  Copyright © 2018 Tindav. All rights reserved.
//

import Foundation
import UIKit
class Utilities {
    
    /*static func findWeekname(weekday: Int) -> String {
     switch weekday {
     case 1:
     return WEEKDAYS.SUN.rawValue
     case 2:
     return WEEKDAYS.MON.rawValue
     case 3:
     return WEEKDAYS.TUE.rawValue
     case 4:
     return WEEKDAYS.WED.rawValue
     case 5:
     return WEEKDAYS.THU.rawValue
     case 6:
     return WEEKDAYS.FRI.rawValue
     case 7:
     return WEEKDAYS.SAT.rawValue
     default:
     return ""
     }
     }*/
    
    static func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func sgbDateFormater(format: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter
    }
    
    static func sgbTimeFormater(format: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        return dateFormatter
    }
    
    // MARK: - Logging
    class func dPrint(_ items: Any...) {
        #if DEBUG
        debugPrint(items.first ?? [])
        #endif
    }
    
    // MARK: - View Utilities
    class func getTopNavigationController() -> UINavigationController? {
        guard let appDelegate = UIApplication.shared.delegate
        else { print("Shared Delegate was nil"); return nil }
        
        guard let window = appDelegate.window
        else { print("AppDelegate Window was nil"); return nil }
        
        guard let rootController = window?.rootViewController
        else { print("AppDelegate Window RootViewController was nil"); return nil }
        
        guard rootController.isKind(of: UINavigationController.self)
        else { print("RootViewController is not of kind UINavigationController"); return nil }
        
        return rootController as? UINavigationController
    }
    
    func getTopViewController() -> UIViewController? {
        var currentVC:UIViewController?
        if let appDelegate = UIApplication.shared.delegate {
            
            guard let window = appDelegate.window else { return nil }
            guard let rootController = window?.rootViewController else { return nil }
            
            let rootVC = rootController as UIViewController
            if rootVC.isKind(of: UINavigationController.self) {
                guard let rootVC = rootVC as? UINavigationController, let cVC = rootVC.visibleViewController else { return nil }
                currentVC = cVC
                return currentVC
            }
            
            //If it is generic View controller then loop though to get presented view controller
            while currentVC?.presentedViewController != nil {
                return (currentVC?.presentedViewController)
            }
        }
        return nil
    }
    
    class func validate(text : String, maxLength: Int?) -> Bool {
        if text.count < 3 {
            return false
        }
        if let maxLength = maxLength, maxLength != 0 {
            if text.count > maxLength {
                return false
            }
        }
        return true
    }
    
    //Funciton to validate accept only numbers
    class func validateNumber(number : String, maxLength: Int) -> Bool {
        if number.count < 3 {
            return false
        }
        let numberRegEx = "[0-9]{\(maxLength)}"
        let numberTest = NSPredicate(format: "SELF MATCHES %@",numberRegEx)
        return numberTest.evaluate(with: number)
    }
    
    
    //Function to validate emails
    class func isValid(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    //Function to validate valid password and must have aleast one small, caps, digit and spl
    class func isValid(password:String, maxLength: Int) -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{\(maxLength),}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
    
    //Function to validate password and confirm password
    class func isBothPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    //Making somepart of text as bold to be used in lables
    class func addBoldText(fullString: String, boldPartOfString: String) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14)]
        let boldFontAttribute = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 14)]
        let boldString = NSMutableAttributedString(string: fullString, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: (fullString as NSString).range(of: boldPartOfString))
        return boldString
    }
    
    /*static func changeInactiveBackgroundColor(button: UIButton) {
     button.backgroundColor = UIColor.white
     button.setTitleColor(UIColor.init(hexString: ColorCodes.BLUE), for: .normal)
     }
     
     static func changeActiveBackgroundColor(button: UIButton) {
     button.backgroundColor = UIColor.init(hexString: ColorCodes.BLUE)
     button.setTitleColor(UIColor.white, for: .normal)
     }*/
    
    static func noInterentConnectionAlert(message: String? = nil, viewController: UIViewController) {
        var messageString = NO_INTERNET_MESSAGE
        if let messageValue = message {
            messageString = messageValue
        }
        Alert().showAlert("Tindav", message: messageString, okButtonTitle: "OK", okButtonStyle: .default, CompletionHandler: nil, View: viewController)
    }
    
    static func getLocalTimeZoneOffset() -> String {
        let offsetValue = (TimeZone.current.secondsFromGMT() / 3600)
        return "\(offsetValue)"
        /*var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
         print("LocalTime Zone Abbreviation \(localTimeZoneAbbreviation)")
         if let regex = try? NSRegularExpression(pattern: "[A-Z]", options: .caseInsensitive) {
         let modString = regex.stringByReplacingMatches(in: localTimeZoneAbbreviation, options: [], range: NSRange(location: 0, length:  localTimeZoneAbbreviation.count), withTemplate: "")
         return modString
         }
         return localTimeZoneAbbreviation*/
    }
}

class TableViewHelper {
    class func EmptyMessage(message:String, tableView:UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-32, height: tableView.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.sizeToFit()
        tableView.backgroundView = messageLabel
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

func authorizationHeader(user: String, password: String) -> (key: String, value: String)? {
    guard let data = "\(user):\(password)".data(using: .utf8) else { return nil }
    
    let credential = data.base64EncodedString(options: [])
    
    return (key: "Authorization", value: "Basic \(credential)")
}
func dicValueString(_ dic:[String : Any]) -> String?{
    let data = try? JSONSerialization.data(withJSONObject: dic, options: [])
    let str = String(data: data!, encoding: String.Encoding.utf8)
    return str
}

// MARK: string to dictionary
func stringValueDic(_ str: String) -> [String : Any]?{
    let data = str.data(using: String.Encoding.utf8)
    if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
        return dict
    }
    return nil
    
}
