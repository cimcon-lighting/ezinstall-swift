//
//  Constant.swift
//  Vizsafe
//
//  Created by Akash Mehta on 29/06/21.
//

import Foundation
import UIKit
import Reachability

var appDelegate         = UIApplication.shared.delegate as! AppDelegate

let ALERT_TITLE         = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
let ALERT_OK_TITLE      = "OK"
let ALERT_CONFIRM_TITLE = "Confirm"
let ALERT_CANCEL_TITLE  = "Cancel"
let DETAILS             = "detail"
let MESSAGE             = "msg"
let ERROR               = "error"
let STATUS              = "status"
let TOKEN               = "token"
let DATA                = "data"

let EXTERNAL            = "external"
let NO_DATA_FOUND       = "No data found"
let OOPS_MSG            = "Oops! Something went wrong"
let NO_INTERNET_MESSAGE = "You must be online to use this feature"
let FORGOT_PWD_MSG      = "Your password has been successfully sent to your Email address"
let TOC_MSG             = "Agree to our Terms of Use?"
let LOCATION_ERROR      = "Turn On Location Service to Allow \"EZinstall\" Determine Your Location"

let DATE_FORMAT         = "MM-dd-yyyy"

struct ThemeColor {
    static let THEME_BLUE_COLOR = "#0072BC"
}

struct ColorCodes {
    static let BUTTON_BLUE = "#346599"
}

struct HTTP_Methods {
    static let POST     = "POST"
    static let GET      = "GET"
}

struct NetworkError {
    static let msgInvalidUsernameAndPassword = "Incorrect Username or Password, try again."
    static let msgNetworkError               = "Network Failed"
}

struct NotificationNames {
   static let NOTIFICTION_NO_NETWORK            = "noNetWork"
   static let NOTIFICATION_REACHABLE            = "network_Reachable"
   static let NOTIFICATION_UPDATED_TOKEN        = "NOTIFICATION_UPDATED_TOKEN"
    static let NOTIFICATION_BOOKING_TABBAR      = "NOTIFICATION_BOOKING_TABBAR"
}

struct CheckNetwork {
    static func isNetworkAvailable() -> Bool {
        var isAvailable  = false;
        
        let reachability = try! Reachability()
        
        switch reachability.connection {
        case .wifi:
            isAvailable = true;
            break;
        case .cellular:
            isAvailable = true;
            break;
        case .none:
            isAvailable = false;
            break;
        case .unavailable:
            isAvailable = false;
        }
        return isAvailable;
    }
}


struct StoryboardIdentifier {
    static let LoginIdentifier      = "loginIdentifier"
    static let ScanSLCIdentifier    = "scanslcIdentifier"
    static let LocationIdentifier   = "LocationIdentifier"
    static let AddressIdentifier    = "addressIdentifier"
    static let CameraIdentifier     = "cameraIdentifier"
    static let NotesIdentifier      = "notesIdentifier"
    static let PoleIDIdentifier     = "poleIdIdentifier"
    static let DetailsIdentifier    = "detailIdentifier"
}
