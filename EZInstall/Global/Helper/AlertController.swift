//
//  AlertController.swift
//  Tindav
//
//  Created by Ghouse Basha Shaik on 18/07/18.
//  Copyright © 2018 Tindav. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    
    var alert : UIAlertController?
    
    func showAlert(_ title: String? = nil,
                   message: String? = nil,
                   okButtonTitle: String? = nil,
                   okButtonStyle: UIAlertAction.Style = .default,
                   okButtonHandler:((UIAlertAction?) -> Void)? = nil ,
                   cancelButtonTitle: String? = nil,
                   cancelButtonStyle: UIAlertAction.Style = .default,
                   cancelButtonHandler:((UIAlertAction?) -> Void)? = nil,
                   CompletionHandler:(() -> Void)? = nil,
                   View : UIViewController) {
        
        if (cancelButtonTitle == nil && okButtonTitle == nil) {
            dismissAlert()
        }
        
        alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        if okButtonTitle != nil { alert?.addAction(UIAlertAction(title: okButtonTitle, style: okButtonStyle, handler: okButtonHandler)) }
        if cancelButtonTitle != nil { alert?.addAction( UIAlertAction(title: cancelButtonTitle, style: cancelButtonStyle, handler: cancelButtonHandler)) }
        
        if View.presentedViewController == nil {
            DispatchQueue.main.async(execute: {
                guard let nAlert = self.alert else { return }
                View.present(nAlert, animated: true, completion: CompletionHandler)
            })
        }
        
        // Auto dismiss alert with delay if no buttons were added
        if (cancelButtonTitle == nil && okButtonTitle == nil) {
            // Delay the dismissal by 1 seconds
            let delay = (2 * Double(NSEC_PER_SEC))
            let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                self.alert?.dismiss(animated: true, completion: CompletionHandler)
            })
        }
    }
    
    func dismissAlert(_ Completion_Handler:(() -> Void)? = nil) {
        alert?.dismiss(animated: true, completion: Completion_Handler)
    }
    
    func showAlertwithtextfiled (title: String? = nil,
                                 message: String? = nil,
                                 inputPlaceholder : String? = nil,
                                 okButtonTitle: String? = nil,
                                 cancelButtonTitle: String? = nil,
                                 View : UIViewController,
                                 CompletionHandler:(() -> Void)? = nil,
                                 cancelButtonHandler:((UIAlertAction?) -> Void)? = nil,
                                 okButtonHandler:((_ text: String?) -> Void)? = nil
                                 ) {
        if (cancelButtonTitle == nil && okButtonTitle == nil) {
            dismissAlert()
        }
        
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert?.addTextField { (textField) in
            textField.placeholder = inputPlaceholder
            textField.autocapitalizationType = .allCharacters
        }
        
        if cancelButtonTitle != nil { alert?.addAction( UIAlertAction(title: cancelButtonTitle, style: .default, handler: cancelButtonHandler)) }
        if okButtonTitle != nil { alert?.addAction(UIAlertAction(title: okButtonTitle, style: .default, handler: { (action:UIAlertAction) in
            guard let textField =  self.alert?.textFields?.first else {
                okButtonHandler?(nil)
                return
            }
            okButtonHandler?(textField.text)
        }))
        }
        
        if View.presentedViewController == nil {
            DispatchQueue.main.async(execute: {
                guard let nAlert = self.alert else { return }
                View.present(nAlert, animated: true, completion: CompletionHandler)
            })
        }
        
        if (cancelButtonTitle == nil && okButtonTitle == nil) {
            // Delay the dismissal by 1 seconds
            let delay = (2 * Double(NSEC_PER_SEC))
            let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                self.alert?.dismiss(animated: true, completion: CompletionHandler)
            })
        }
    }
}
