//
//  WebConstant.swift
//  Vizsafe
//
//  Created by Akash Mehta on 29/06/21.
//

import Foundation

let PROD_BASE_URL   = "https://clapptest.cimconlms.com/SLCScannerV2/api-bct/"

let DEV_BASE_URL    = "https://clapptest.cimconlms.com/SLCScannerV2/api-bct/"

let BASE_URL        =  DEV_BASE_URL

let SECURITY_URL    = BASE_URL + "checkuniquecode"
let LOGIN_URL       = BASE_URL + "userLogin"
let SLCLIST_URL     = BASE_URL + "slclist"
let SLC_DETAILS     = BASE_URL + "slc"
let CHECKMACINTERNAL = BASE_URL + "checkInternalUniqueMacAddressAPI"
let CHECKMACADDRESS = BASE_URL + "checkmacaddress"
let CHECKSLCID      = BASE_URL + "checkslcid"
let CHECKMACANDSCLID = BASE_URL + "checkMacAddressSlcIdbeforeSave"
let ADDRESS         = BASE_URL + "address"
