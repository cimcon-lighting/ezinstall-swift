//
//  ErrorMessage.swift
//  Vizsafe
//
//  Created by Akash Mehta on 29/06/21.
//

import Foundation

import Foundation
struct ValidationError {
    var errorString: String!
    init(message: String) {
        errorString = message
    }
    
    struct ErrorMessages {
        static let msgEmptySecuritycode = "Please enter security code"
        static let msgEmptyUsername     = "Please enter username"
        static let msgEmptyPassword     = "Please enter password"
        
    }
}

