//
//  NetworkExtention.swift
//  Vizsafe
//
//  Created by Akash Mehta on 30/06/21.
//

import Foundation
import UIKit
import UserNotifications

//======================== NOTIFICATION ========================================
extension Notification.Name {
    static let notificaton_NoNetwork = Notification.Name(NotificationNames.NOTIFICTION_NO_NETWORK)
    static let notificaton_Network_Reachable = Notification.Name(NotificationNames.NOTIFICATION_REACHABLE)
     static let notificaton_updated_Token = Notification.Name(NotificationNames.NOTIFICATION_UPDATED_TOKEN)
    static let notification_booking_tabbar = Notification.Name(NotificationNames.NOTIFICATION_BOOKING_TABBAR)
}

//======================== URLResponse EXTENSION ========================================
extension URLResponse {
    func getStatusCode() -> Int? {
        if let httpResponse = self as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
}
