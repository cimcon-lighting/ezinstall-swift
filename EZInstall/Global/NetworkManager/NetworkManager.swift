//
//  NetworkManager.swift
//  Tindav
//
//  Created by Ghouse Basha Shaik on 18/07/18.
//  Copyright © 2018 Tindav. All rights reserved.
//

import Foundation
import MBProgressHUD

enum NetworkConnection {
    case available
    case notAvailable
}

class NewtworkManager {
    private init() {}
    static let shared = NewtworkManager()
    
    // MARK: - No Internet Connection
    func checkInternetConnection() -> NetworkConnection {
        if CheckNetwork.isNetworkAvailable() {
            return .available
        }
        return .notAvailable
    }
    
    
    func makeAPICalls(apiRequest: APIRequest, isLoaderShow: Bool ,completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        switch checkInternetConnection() {
        case .available:
            if isLoaderShow {
                DispatchQueue.main.async {
                    let loadingNotification = MBProgressHUD.showAdded(to:  appDelegate.window!, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                }
            }
            
            //Making a API Call
            let config = URLSessionConfiguration.ephemeral
            if let headers = apiRequest.headers {
                config.httpAdditionalHeaders = headers
            }
            let session = URLSession(configuration: config)
            
//            let HTTPHeaderField_ContentType         = "content-type"
//            let ContentType_ApplicationJson         = "application/json"
            let HTTPMethod_Get                      = apiRequest.httpMethod
            
            let callURL = URL.init(string: apiRequest.urlString)
            var request = URLRequest.init(url: callURL!)
            request.timeoutInterval = 60.0 // TimeoutInterval in Second
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = HTTPMethod_Get
            
            if let httpHeaders = apiRequest.headers {
                request.allHTTPHeaderFields = httpHeaders//httpBody.dataUsingEncoding(NSUTF8StringEncoding)
            }
            
            if let httpBody = apiRequest.httpBody {
                request.httpBody = httpBody as Data//httpBody.dataUsingEncoding(NSUTF8StringEncoding)
            }
            
            print("\n URL : \(apiRequest.urlString) \n HEADERS : \(apiRequest.headers ?? [:]) \n BODY PARAMS : \(String(decoding: apiRequest.httpBody ?? Data(), as: UTF8.self)) \n ")

            if let parameters = apiRequest.parameters, parameters.count > 0 {
                //request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
                do {
                    let json = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) as Data
                    print(json)
                    if let jsonString = String(data: json, encoding: .utf8) {
                        print(jsonString)
                    }
                    request.httpBody = json
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            
            let task = session.dataTask(with: request as URLRequest) { (data, response, err) in
                var dataNew : Data? = nil
                do{
                    if isLoaderShow {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                        }
                    }
                    if err != nil {
                        DispatchQueue.main.async {
                            Alert().showAlert(ALERT_TITLE, message: err?.localizedDescription, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
                        }
                    } else if data != nil {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                        print("RESPONSE : \(json ?? [:]) code : \(response?.getStatusCode()!)")
                        if response?.getStatusCode()! == 200 {
                            do{
                                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                                if let aJson = json {
//                                    print("RESPONSE : \(aJson)")
                                    if aJson[STATUS] as? String == ERROR || aJson[STATUS] as? String == "-1" {
                                        DispatchQueue.main.async {
                                            Alert().showAlert(ALERT_TITLE, message: aJson[MESSAGE] as? String ?? OOPS_MSG, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
                                        }
                                    } else {
                                        if let aData = dicValueString(aJson)?.data(using: .utf8) {
                                            dataNew = aData
                                        }
                                    }
                                }
                            } catch {
                                DispatchQueue.main.async {
                                    Alert().showAlert(ALERT_TITLE, message: "Network failed. Check Parsing", okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
                                }
                            }
                        } else {
                            do{
                                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                                DispatchQueue.main.async {
                                    Alert().showAlert(ALERT_TITLE, message: (json?[MESSAGE] as? String ?? OOPS_MSG), okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
                                }
                                // failureHandler((json?[MESSAGE] ?? OOPS_MSG) as! String)
                            } catch {
                                DispatchQueue.main.async {
                                    Alert().showAlert(ALERT_TITLE, message: OOPS_MSG, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
                                }
                            }
                        }
                    }
                }catch{ print("RESPONSE WRONG") }
                completion(dataNew, response, err)
            }
            task.resume()
            
        case .notAvailable:
            completion(nil, nil, (Error.self) as? Error)
            if isLoaderShow {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                }
            }
            Alert().showAlert(ALERT_TITLE, message: NO_INTERNET_MESSAGE, okButtonTitle: ALERT_OK_TITLE, CompletionHandler: nil, View: appDelegate.window!.rootViewController!)
        }
    }
}
